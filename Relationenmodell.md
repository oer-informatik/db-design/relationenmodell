## Der logische Datenbankentwurf mit dem Relationenmodell

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/relationenmodell</span>

> **tl/dr;** _(ca. 8 min Lesezeit): Im logischen Modell legen wir uns fest, wie die logische Verknüpfung der Daten untereinander umgesetzt werden soll. Im Relationenmodell erfolgt dies über Schlüsselattribute. Dieser Artikel erklärt die unterschiedlichen Wege, Schlüsselattribute zu bestimmen, die Transformationsregeln, um die Beziehungstypen des Entity-Relationship-Modells in Schlüsselbeziehungen umzusetzen und führt Constraints ein, die Attributen im logischen Modell zugeordnet werden._

<span class="kann-liste">

Nach Durchlesen des Artikels kann ich ein Entity-Relationship-Modell in ein Relationenmodell überführen, in dem ich:

- ... Entitätstypen in Relationenschema überführe,

- ... Primärschlüsselkandidaten identifiziere und daraus Primärschlüssel bestimme,

- ... Beziehungen des Entity-Relationship-Modells mit unterschiedlicher Kardinalität und Optionalitäten in Schlüsselbeziehungen transformieren.

- ... bestehende Constraints (Optionalität, abgeleitete Attribute, schwache Schlüssel) in ein Relationenmodell überführe und

- ... notwendige neue Constraints (unikal, eingabepflichtig) in dem Modell ergänze.

</span>

### Vom konzeptuellen ins logische Modell

Im [konzeptuellen Modell (dem _Entity Relationship Model_)](https://oer-informatik.de/erm) wurden die abzubildenden Daten aus der realen Welt in Form von Entitätstypen und Attributen abstrahiert und strukturiert. Dabei erfolgte noch keine Festlegung in welcher Art Datenbank die Daten gespeichert werden sollen: ob beispielsweise als XML-Datei, in einer hierarchischen Datenbank oder eben einer relationalen Datenbank. Diese Festlegung auf ein logisches Datenbankmodell erfolgt zu Beginn des nächsten Entwurfsschritts: im logischen Datenmodell.

![Schrittweiser Entwurf einer Datenbank](images/Datenbankentwurfsphasen.png)

Es gibt eine Vielzahl unterschiedlicher Datenmodelle mit einer noch größeren Vielzahl konkreter Programme (Implementierungen). Die bekanntesten Vertreter sind:

* Hierarchisches Modell
  (z.B. Dateisystem – jeder Datensatz hat genau einen Vorgänger)

* Netzwerkmodell
  (mehrere Vorgängerdaten, Struktur bei Datenabfrage festgelegt)

* Relationales Modell
  (in Beziehung stehende Tabellen)

* Objektorientiertes Modell
  (Objekte aus OOP können in DB gespeichert werden)

* Objekt-relationales Modell
  (relationales Modell, das Vererbung zulässt)

* Semistrukturiertes Modell / schemafreie DB
(Struktur der Datenelemente ist nicht festgelegt (z.B. schemafreies XML, JSON)

### Transformation des konzeptuellen Modells in ein relationales logisches Modell: das Relationenmodell

#### Informationen aus dem ER-Modell

Welche Eigenschaften werden im _Entity-Relationship-Modell_ beschrieben, die übertragen werden müssen?

* Entitätstypen

* Attribute

* Beziehungen (1:1, 1:N, M:N)

* Optionalitäten der Beziehungen (1:C, 1:NC, M:NC)

* Mehrwertigkeit von Attributen

* Schwache Entitätstypen und identifizierende Relationen

* Schwache Schlüsselattribute

* abgeleitete Attribute

#### Transformationsschritte

##### Primärschlüssel wählen

Für jeden Entitätstyp muss ein Primärschlüssel (_primary key_) gefunden werden, der jede Entität eindeutig identifiziert.

Primärschlüssel können sich aus einem oder einer Kombination mehrerer Attribute ergeben. Primärschlüsselkandidat sind alle Attribut(-kombinationen), die für jede einzelne Entität einzigartig (_unique_) ist. Geeignete zusammengesetzte Primärschlüsselkandidaten enthalten nur so viele Attribute, wie zur Identifizierung einer Entität erforderlich. Neben den natürlichen Schlüsseln, die sich aus vorhandenen Attributen ergeben, besteht auch noch die Möglichkeit, künstliche Primärschlüssel zu vergeben. Diese sogenannten Surrogatschlüssel können beispielsweise fortlaufende Nummern oder anderweitig erzeugte (Hash-)werte sein.

Aus den so gebildeten Schlüsselkandidaten kann ein Primärschlüssel ausgewählt werden.

##### Kardinalitäten in Fremdschlüsselbeziehungen transformieren

Die Beziehungen zwischen Entitätstypen werden im ER-Modell als Assoziation mit Kardinalitäten modelliert.

Im Relationenmodell werden sie in Fremdschlüsselbeziehungen transformiert:  die Primärschlüssel einer Relation werden in einer zweiten Relation als Attribut gelistet - dort erhalten sie die Bezeichnung "Fremdschlüssel" (_foreign key_).

Die ersten Transformationsschritte sind für alle Arten von Entitätstypen identisch:

1. Aus allen Entitätstypen werden Relationen. Oft werden diese ale einfacher Text erfasst, klein geschrieben und mit dem Plural bezeichnet: `schülerInnen()`

2. Alle zugehörigen Attribute der Entitätstypen werden hinter dem Relationennamen in Klammern kommagetrennt gelistet: `schülerInnen(SchülerInID, Vorname, Nachname)`

3. Die Primärschlüsselattribute werden unterstrichen oder mit PK (für _primaty key_) annotiert: `schuelerInnen(SchülerInID (PK), Vorname, Nachname)`


###### Transformation einer Assoziation mit `1:N` - Kardinalität

Als Beispiel dient die Assoziation zwischen den Entitätstypen `Schüler*in` und `Klasse`:

![1:n Beziehung zwischen SchülerIn und LehrerIn](images/1-n_Schueler_Klasse_Chen.png)

Zunächst werden beide Entitätstypen wie oben unter 1.-3. beschreiben in Relationen umgewandelt, dann folgt die Umwandlung der Assoziation und Kardinalität:

4. Bei `1:N`-Beziehungen wird der Primärschlüssel der Realtion, bei der die Kartinalität "1" steht als  Fremdschlüsselattribut in die Relation, bei der das "N" steht aufgenommen. Fremdschlüsselattribute werden mit Pfeil nach oben gekennzeichnet oder mit FK (_foreign key_) annotiert:

**Relationenschema für 1:N-Beziehungen**

 - schülerInnen (<U>SchülerInID</U>, Vorname, Nachname &#8593;Klassenbezeichnung&#8593;)

 - klassen (<U>Klassenbezeichnung</U>, KlassenlehrerIn)

oder in Textform ohne formatierungen und Sonderzeichen:

```
schülerInnen (SchülerInID (PK), Vorname, Nachname, Klassenbezeichnung (FK))

klassen (Klassenbezeichnung (PK), KlassenlehrerIn)
```

######  Transformation einer Assoziation mit `M:N` - Kardinalität

Eine Assoziation mit einer Vielfachheit zu beiden Seiten (`M:N`) wird am Beispiel der Entitätstypen `Lehrer*in` und `Klasse` dargestellt:

![m:n-Beziehung zwischen Klasse und LehrerIn](images/m-n_Lehrer_Klasse_Chen.png)

Auch hier werden zunächst beide Entitätstypen wie oben unter 1.-3. beschreiben in Relationen umgewandelt, danach folgt die Umwandlung der Assoziation und Kardinalität:

5. Im Fall einer `M:N`-Beziehung wird eine neue Relation gebildet. Diese zusätzliche Relation erhält die Primärschlüssel beider verknüpfter Relationen als zusammengesetzten Primärschlüssel. Diese beiden Attribute sind zugleich Fremdschlüssel.

**Relationenschema für `M:N`-Beziehungen**

- lehrerInnen (<U>LehrerInID</U>, Vorname, Nachname)

- klassen (<U>Klassenbezeichnung</U>, Jahrgangsstufe)

- unterrichte (&#8593;<U>LehrerInID</U>&#8593;, &#8593;<U>Klassenbezeichnung</U>&#8593;)

bzw. in Reintextform:

```
lehrerInnen (LehrerInID (PK), Vorname, Nachname)
klassen (Klassenbezeichnung (PK), Jahrgangsstufe)
unterrichte (LehrerInID(PK, FK), Klassenbezeichnung(PK, FK))
```

######  Transformation einer Assoziation mit 1:1 - Kardinalität

Relativ selten kommt es in der Praxis vor, dass zwei Entitätstypen in einer `1:1`-Beziehung zueinander stehen. Bei der Umwandlung in das Relationenmodell ergeben sich hier eine Reihe von Möglichkeiten, die anhand des Beispiels `Einwohner` und `Personalausweis` dargestellt werden:

![1:1 Relation zwischen Einwohner und Personalausweis](images/1-1_Perso-Einwohner_Chen.png)

**Möglichkeit 1**: Zusammenfassung in einer Relation

In der Regel ist es am einfachsten, die beiden Entitätstypen in einer gemeinsamen Relation zusammenzufassen, wobei nur einer der beiden Primärschlüssel in der neuen Relation diese Funktion übernimmt (der zweite bleibt als _unikales_ - also einzigartiges - Attribut vorhanden).

- einwohner (<U>SozialversicherungsNr</U>, Vorname, Nachname, PersoNr (unikal), Ausstellungsdatum, Gültigkeit)

**Möglichkeit 2**: zwei Relationen mit gemeinsamem Primärschlüssel

Denkbar ist auch, dass zwei Relationen gebildet werden, die beide den gleichen Primärschlüssel verwenden. Diese Variante wäre zu bevorzugen, wenn einer der beiden Entitätstypen sehr selten zugeordnet würde und dieser vergleichsweise teuer ist (also Daten-/Ressourcen-aufwändig). Würde also beispielsweise ein hochaufgelöstes Passbild gespeichert bei den wenigen Einträgen der Personalausweise, wobei sehr viele Einwohner ohne Personalausweis existieren, dann wäre dies der Weg der Wahl.

- einwohner (&#8593;<U>SozialversicherungsNr</U>&#8593;, Vorname, Nachname)

- personalausweise (&#8593;<U>SozialversicherungsNr</U>&#8593;, PersoNr (unikal), Ausstellungsdatum, Gültigkeit)

**Möglichkeit 3**: Primärschlüssel als Fremdschlüssel, dieser muss _unikal_ sein

Eine dritte, aber fehleranfälligere Variante geht von zwei Relationen mit unterschiedlichen Primärschlüsseln aus, die über einen Fremdschlüssel verknüpft sind. Damit auf diesem Weg keine `1:N`-Beziehung modelliert wird und die Datenbank inkonsistent wird muss dieser Fremdschlüssel jedoch _unikal_, also in der Eingabe einzigartig, sein. Diesen Weg sollte man nur wählen, wenn man weiß, was man tut.


- einwohner (&#8593;<U>SozialversicherungsNr</U>&#8593;, Vorname, Nachname)

- personalausweise (<U>PersoNr</U>,  &#8593;SozialversicherungsNr&#8593; (unikal),  Ausstellungsdatum, Gültigkeit)

**Möglichkeit 4**: Verknüpfungstabelle mit zusammengesetztem Primärschlüssel, in dem beide Teilschlüssel _unikal_ sein müssen

Eine vierte Variante ist sachgerecht, wenn beide Entitätstypen optional sind, es also im Beispiel sowohl Einwohner ohne Personalausweis als auch Personalausweise ohne Einwohner gibt. Hierbei wird analog zur M:N-Beziehung eine Zwischenrelation erstellt, bei der beide Primärschlüssel der Einzelrelationen als Fremdschlüssel übernommen werden und gemeinsam den zusammengesetzten Primärschlüssel der neuen Relation ergeben. Um Mehrfachzuordnungen auszuschließen ist auch hier wichtig, dass die beiden Primärschlüsselteile in der Zwischentabelle _unikal_ sind.

- einwohner (<U>SozialversicherungsNr</U>, Vorname, Nachname)

- personalausweise (<U>PersoNr</U>,  Ausstellungsdatum, Gültigkeit)

- einwohner2personalausweise (&#8593;<U>PersoNr</U>&#8593; (unikal),  &#8593;<U>SozialversicherungsNr</U>&#8593; (unikal))


**Die hier in allen Beispielen genannte Einzigartigkeit (unikal / _unique_) muss im Relationenmodell explizit genannt werden!**

#### Transformation von schwachen Entitätstypen mit identifizierender Assoziation

![Schwacher Entitätstyp mit identifiziernder Assoziation und schwachem Schlüssel](images/SchwacheEntitätstypen_Chen.png)

Sofern zur Identifizierung einer Entität neben einem Attribut des Entitätstyps eine Beziehung zu einem anderen Entitätstyp nötig ist, spricht man von einem _schwachen Entitätstypen_. Die Rechnungsposition "1" existiert auf mehreren Rechnungen, daher ist zur genauen Identifizierung die Kenntnis wichtig, um welche Rechnung es sich handelt. `Rechnungsposition` ist in diesem Fall ein _schwacher Schlüssel_, `Rechnungsposten` ein _schwacher Entitätstyp_ und `enthält` ist eine _identifiziernde Beziehung_.

Der schwache Entitätstyp wird in diesem Fall als Relation mit zusammengesetztem Primärschlüssel modelliert, wobei der eine Teilschlüssel dem Fremdschlüssel aus der identifizierenden Beziehung entspricht und der zweite Teilschlüssel der schwache Schlüssel des Ausgangs-Entitätstyps ist.

Relationenschema für schwache Relationen in `1:N`-Beziehungen:

- rechnungen(<U>Rechnungsnummer</U>, Rechnungsanschrift, Datum)

- rechnunsposten (<U>Rechnungsposition</U>, &#8593;<U>Rechnungsnummer</U>&#8593;, Preis, Anzahl, Artikel)


##### Optionalitäten transformieren

Bei Kardinalitäten, die mit Optionalität formuliert sind (1:C, 1:NC, N:MC) wird die Optionalität in der Regel über die nicht verpflichtende Eingabe eines Fremdschlüssels modelliert.
Primärschlüsselattribute gelten immer als eingabepflichtig, alle anderen Attribute (inkl. Fremdschlüssel) gelten als nicht eingabepflichtig.

Sofern für Nichtschlüsselattribute eine Eingabepflicht festgelegt werden soll kann dies durch explizite Angabe von `NOT NULL` dokumentiert werden, einige CASE-Tools notieren Eingabepflichtige Attribute **fett**, Attribute ohne Eingabepflicht in normaler Schreibweise.

##### Alternative Schreibweisen

Da sowohl die Pfeile für Fremdschlüssel als auch die Unterstreichungen für Primärschlüssel nicht in jedem System darstellen lassen, gibt es eine Reihe abweichender Notationsformen für Relationenmodelle.

Am häufigsten anzutreffen ist die Notation als Tabelle mit Kennzeichnung der Schlüssel über Schlüsselsymbole oder "FK" und "PK". Einzigartigkeit wird dann häufig mit `uniq`, Eingabepflicht mit `NOT NULL` oder Sternchen `*` angegeben. Viele CASE-Tools zum Datenbankentwurf setzen dies so um, auch in den IHK Abschlussprüfungen für Fachinformatiker*innen ist dies häufig so anzutreffen:

![Darstellung des Relationenmodells als Tabelle](images/RelationenmodellAlsTabelle.png)

Häufig wird zur Verdeutlichung der Fremdschlüsselbeziehungen noch eine Verbindungslinie mit Martin-notierten Kardinalitäten ("Krähenfuß") ergänzt. Diese Information ist aber redundant, da die Fremdschlüssel bereits die Beziehungen wiedergeben.

##### PlantUML für Relationenschemata verwenden:

Mit dem UML-Werkzeug plantUML lassen sich auch Relationenschema mit Schlüsselbeziehungen abbilden. Bei einigen Symbolen muss auf die HTML-notierten Unicode-Zeichen zurückgegriffen werden. In folgendem Beispiel wurden bewusst mehrere redundante Wege der Kennzeichnung aufgezeigt - natürlich genügt zu Dokumentationszwecken eine der genannten Hervorhebungen:

![Darstellung des Relationenmodells als Tabelle](images/plantUML-Relationenmodell.svg)


Erstellt werden können die plantUML-Diagramme beispielsweise über den Webdienst [planttext (wie z.B. die obige Grafik über diesen Link)](https://www.planttext.com/?text=dLJHJjj047ptLwo2H1D4rQQeKe4eWa0ZeKOkhSD3dz3PtjYddE_SlJLMKl4t_GpU-B6kCQMceHNYdYnvRdPkPkwEF2lYgZ11hbE4H5DYCF1BRKj5ge34AE-FcKd75UDPucoarmXlEv1JNAZAyDHP3bM1q9jLYKuLHCfwcAzAh4apKg-pvQHX_H60h8cZ5Ed46KTmgcWP6SKuCHMsVrMopCXLDcqHikfSs7pXbXXSToX3n_WInfdESlw6nhZw4TynaQkV8da5wihf54foiS72Gwqv1sKpWoaaiid30f4xL4PRPF41777kWW0jQrv1xoickQrivald6MqF6ltRM_lxm-4XT7zgrix2Q7J-QJpkGBQtFhpVt-j0Gxe7ZKRJ5he3etXyJAoNQ6nL54YZDvEneCBFSmWlPhCxr0HzaXDAn3Ph-VU6xmuXAb4dET8QDSkMY_3iYy2lWy2Arq2DSU0MyDSn3Wusf0Vdf8lRdoHJpEsDzsYW4Cf8Me5Ed85-Ati04Wj4go9s1lhtnnn0XakqTjs84IF5lXGQ1S_N4L8mHsqwurFqi27AxqAWZ8SfOP7-J-lq2QrtKRnSwwECWe_QPYh6Sc64XtKcQd_VGRYzYP4U1kyCTgLkpA-ZDi4QjSTMMbVndxW788bUESemTp7QvojyL19NDhagWm-w46gKUsV1DfB51--iN4D-wRXk-He_rZgJXqgiw4piNrPMVxySR2RKTdMZJqsj_st0aNmsB-4l) [IDE-Plugins oder git*-Dienste wie bitbucket, gitlab oder github](https://plantuml.com/de/running).

Der Quelltext zur Erstellung des obigen Relationenschemas wäre etwa:

```plantuml
@startuml
hide circle
skinparam classAttributeIconSize 0

entity "entity" {
  &#9911; PrimaryKey attribute <<PK>>
  &#8593; ForeignKey attribute <<FK>>
  * <b>mandatory attribute</b> <<NOT NULL>>
    optional attribute
  &#9312; unique attribute <<UNIQ>>
}

@enduml
```

#### Links und weitere Informationen

* eine detaillierte Liste aller Transformationsregeln - vor allem inkl. der Sonderfälle für rekursive und optionale Beziehungen findet sich u.a. im "Grundkurs Datenbankentwurf" von Helmut Jarosch, 4. Auflage 2016

* [dbdiagram.io](https://dbdiagram.io/d)  ist ein weiteres Onlinetool, um relationale Datenmodelle darzustellen
